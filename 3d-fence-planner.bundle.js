(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else {
		var a = factory();
		for(var i in a) (typeof exports === 'object' ? exports : root)[i] = a[i];
	}
})(window, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/mustache/mustache.js":
/*!*******************************************!*\
  !*** ./node_modules/mustache/mustache.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*!
 * mustache.js - Logic-less {{mustache}} templates with JavaScript
 * http://github.com/janl/mustache.js
 */

/*global define: false Mustache: true*/

(function defineMustache (global, factory) {
  if ( true && exports && typeof exports.nodeName !== 'string') {
    factory(exports); // CommonJS
  } else if (true) {
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [exports], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)); // AMD
  } else {}
}(this, function mustacheFactory (mustache) {

  var objectToString = Object.prototype.toString;
  var isArray = Array.isArray || function isArrayPolyfill (object) {
    return objectToString.call(object) === '[object Array]';
  };

  function isFunction (object) {
    return typeof object === 'function';
  }

  /**
   * More correct typeof string handling array
   * which normally returns typeof 'object'
   */
  function typeStr (obj) {
    return isArray(obj) ? 'array' : typeof obj;
  }

  function escapeRegExp (string) {
    return string.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
  }

  /**
   * Null safe way of checking whether or not an object,
   * including its prototype, has a given property
   */
  function hasProperty (obj, propName) {
    return obj != null && typeof obj === 'object' && (propName in obj);
  }

  /**
   * Safe way of detecting whether or not the given thing is a primitive and
   * whether it has the given property
   */
  function primitiveHasOwnProperty (primitive, propName) {  
    return (
      primitive != null
      && typeof primitive !== 'object'
      && primitive.hasOwnProperty
      && primitive.hasOwnProperty(propName)
    );
  }

  // Workaround for https://issues.apache.org/jira/browse/COUCHDB-577
  // See https://github.com/janl/mustache.js/issues/189
  var regExpTest = RegExp.prototype.test;
  function testRegExp (re, string) {
    return regExpTest.call(re, string);
  }

  var nonSpaceRe = /\S/;
  function isWhitespace (string) {
    return !testRegExp(nonSpaceRe, string);
  }

  var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
  };

  function escapeHtml (string) {
    return String(string).replace(/[&<>"'`=\/]/g, function fromEntityMap (s) {
      return entityMap[s];
    });
  }

  var whiteRe = /\s*/;
  var spaceRe = /\s+/;
  var equalsRe = /\s*=/;
  var curlyRe = /\s*\}/;
  var tagRe = /#|\^|\/|>|\{|&|=|!/;

  /**
   * Breaks up the given `template` string into a tree of tokens. If the `tags`
   * argument is given here it must be an array with two string values: the
   * opening and closing tags used in the template (e.g. [ "<%", "%>" ]). Of
   * course, the default is to use mustaches (i.e. mustache.tags).
   *
   * A token is an array with at least 4 elements. The first element is the
   * mustache symbol that was used inside the tag, e.g. "#" or "&". If the tag
   * did not contain a symbol (i.e. {{myValue}}) this element is "name". For
   * all text that appears outside a symbol this element is "text".
   *
   * The second element of a token is its "value". For mustache tags this is
   * whatever else was inside the tag besides the opening symbol. For text tokens
   * this is the text itself.
   *
   * The third and fourth elements of the token are the start and end indices,
   * respectively, of the token in the original template.
   *
   * Tokens that are the root node of a subtree contain two more elements: 1) an
   * array of tokens in the subtree and 2) the index in the original template at
   * which the closing tag for that section begins.
   */
  function parseTemplate (template, tags) {
    if (!template)
      return [];

    var sections = [];     // Stack to hold section tokens
    var tokens = [];       // Buffer to hold the tokens
    var spaces = [];       // Indices of whitespace tokens on the current line
    var hasTag = false;    // Is there a {{tag}} on the current line?
    var nonSpace = false;  // Is there a non-space char on the current line?

    // Strips all whitespace tokens array for the current line
    // if there was a {{#tag}} on it and otherwise only space.
    function stripSpace () {
      if (hasTag && !nonSpace) {
        while (spaces.length)
          delete tokens[spaces.pop()];
      } else {
        spaces = [];
      }

      hasTag = false;
      nonSpace = false;
    }

    var openingTagRe, closingTagRe, closingCurlyRe;
    function compileTags (tagsToCompile) {
      if (typeof tagsToCompile === 'string')
        tagsToCompile = tagsToCompile.split(spaceRe, 2);

      if (!isArray(tagsToCompile) || tagsToCompile.length !== 2)
        throw new Error('Invalid tags: ' + tagsToCompile);

      openingTagRe = new RegExp(escapeRegExp(tagsToCompile[0]) + '\\s*');
      closingTagRe = new RegExp('\\s*' + escapeRegExp(tagsToCompile[1]));
      closingCurlyRe = new RegExp('\\s*' + escapeRegExp('}' + tagsToCompile[1]));
    }

    compileTags(tags || mustache.tags);

    var scanner = new Scanner(template);

    var start, type, value, chr, token, openSection;
    while (!scanner.eos()) {
      start = scanner.pos;

      // Match any text between tags.
      value = scanner.scanUntil(openingTagRe);

      if (value) {
        for (var i = 0, valueLength = value.length; i < valueLength; ++i) {
          chr = value.charAt(i);

          if (isWhitespace(chr)) {
            spaces.push(tokens.length);
          } else {
            nonSpace = true;
          }

          tokens.push([ 'text', chr, start, start + 1 ]);
          start += 1;

          // Check for whitespace on the current line.
          if (chr === '\n')
            stripSpace();
        }
      }

      // Match the opening tag.
      if (!scanner.scan(openingTagRe))
        break;

      hasTag = true;

      // Get the tag type.
      type = scanner.scan(tagRe) || 'name';
      scanner.scan(whiteRe);

      // Get the tag value.
      if (type === '=') {
        value = scanner.scanUntil(equalsRe);
        scanner.scan(equalsRe);
        scanner.scanUntil(closingTagRe);
      } else if (type === '{') {
        value = scanner.scanUntil(closingCurlyRe);
        scanner.scan(curlyRe);
        scanner.scanUntil(closingTagRe);
        type = '&';
      } else {
        value = scanner.scanUntil(closingTagRe);
      }

      // Match the closing tag.
      if (!scanner.scan(closingTagRe))
        throw new Error('Unclosed tag at ' + scanner.pos);

      token = [ type, value, start, scanner.pos ];
      tokens.push(token);

      if (type === '#' || type === '^') {
        sections.push(token);
      } else if (type === '/') {
        // Check section nesting.
        openSection = sections.pop();

        if (!openSection)
          throw new Error('Unopened section "' + value + '" at ' + start);

        if (openSection[1] !== value)
          throw new Error('Unclosed section "' + openSection[1] + '" at ' + start);
      } else if (type === 'name' || type === '{' || type === '&') {
        nonSpace = true;
      } else if (type === '=') {
        // Set the tags for the next time around.
        compileTags(value);
      }
    }

    // Make sure there are no open sections when we're done.
    openSection = sections.pop();

    if (openSection)
      throw new Error('Unclosed section "' + openSection[1] + '" at ' + scanner.pos);

    return nestTokens(squashTokens(tokens));
  }

  /**
   * Combines the values of consecutive text tokens in the given `tokens` array
   * to a single token.
   */
  function squashTokens (tokens) {
    var squashedTokens = [];

    var token, lastToken;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      if (token) {
        if (token[0] === 'text' && lastToken && lastToken[0] === 'text') {
          lastToken[1] += token[1];
          lastToken[3] = token[3];
        } else {
          squashedTokens.push(token);
          lastToken = token;
        }
      }
    }

    return squashedTokens;
  }

  /**
   * Forms the given array of `tokens` into a nested tree structure where
   * tokens that represent a section have two additional items: 1) an array of
   * all tokens that appear in that section and 2) the index in the original
   * template that represents the end of that section.
   */
  function nestTokens (tokens) {
    var nestedTokens = [];
    var collector = nestedTokens;
    var sections = [];

    var token, section;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      token = tokens[i];

      switch (token[0]) {
        case '#':
        case '^':
          collector.push(token);
          sections.push(token);
          collector = token[4] = [];
          break;
        case '/':
          section = sections.pop();
          section[5] = token[2];
          collector = sections.length > 0 ? sections[sections.length - 1][4] : nestedTokens;
          break;
        default:
          collector.push(token);
      }
    }

    return nestedTokens;
  }

  /**
   * A simple string scanner that is used by the template parser to find
   * tokens in template strings.
   */
  function Scanner (string) {
    this.string = string;
    this.tail = string;
    this.pos = 0;
  }

  /**
   * Returns `true` if the tail is empty (end of string).
   */
  Scanner.prototype.eos = function eos () {
    return this.tail === '';
  };

  /**
   * Tries to match the given regular expression at the current position.
   * Returns the matched text if it can match, the empty string otherwise.
   */
  Scanner.prototype.scan = function scan (re) {
    var match = this.tail.match(re);

    if (!match || match.index !== 0)
      return '';

    var string = match[0];

    this.tail = this.tail.substring(string.length);
    this.pos += string.length;

    return string;
  };

  /**
   * Skips all text until the given regular expression can be matched. Returns
   * the skipped string, which is the entire tail if no match can be made.
   */
  Scanner.prototype.scanUntil = function scanUntil (re) {
    var index = this.tail.search(re), match;

    switch (index) {
      case -1:
        match = this.tail;
        this.tail = '';
        break;
      case 0:
        match = '';
        break;
      default:
        match = this.tail.substring(0, index);
        this.tail = this.tail.substring(index);
    }

    this.pos += match.length;

    return match;
  };

  /**
   * Represents a rendering context by wrapping a view object and
   * maintaining a reference to the parent context.
   */
  function Context (view, parentContext) {
    this.view = view;
    this.cache = { '.': this.view };
    this.parent = parentContext;
  }

  /**
   * Creates a new context using the given view with this context
   * as the parent.
   */
  Context.prototype.push = function push (view) {
    return new Context(view, this);
  };

  /**
   * Returns the value of the given name in this context, traversing
   * up the context hierarchy if the value is absent in this context's view.
   */
  Context.prototype.lookup = function lookup (name) {
    var cache = this.cache;

    var value;
    if (cache.hasOwnProperty(name)) {
      value = cache[name];
    } else {
      var context = this, intermediateValue, names, index, lookupHit = false;

      while (context) {
        if (name.indexOf('.') > 0) {
          intermediateValue = context.view;
          names = name.split('.');
          index = 0;

          /**
           * Using the dot notion path in `name`, we descend through the
           * nested objects.
           *
           * To be certain that the lookup has been successful, we have to
           * check if the last object in the path actually has the property
           * we are looking for. We store the result in `lookupHit`.
           *
           * This is specially necessary for when the value has been set to
           * `undefined` and we want to avoid looking up parent contexts.
           *
           * In the case where dot notation is used, we consider the lookup
           * to be successful even if the last "object" in the path is
           * not actually an object but a primitive (e.g., a string, or an
           * integer), because it is sometimes useful to access a property
           * of an autoboxed primitive, such as the length of a string.
           **/
          while (intermediateValue != null && index < names.length) {
            if (index === names.length - 1)
              lookupHit = (
                hasProperty(intermediateValue, names[index]) 
                || primitiveHasOwnProperty(intermediateValue, names[index])
              );

            intermediateValue = intermediateValue[names[index++]];
          }
        } else {
          intermediateValue = context.view[name];

          /**
           * Only checking against `hasProperty`, which always returns `false` if
           * `context.view` is not an object. Deliberately omitting the check
           * against `primitiveHasOwnProperty` if dot notation is not used.
           *
           * Consider this example:
           * ```
           * Mustache.render("The length of a football field is {{#length}}{{length}}{{/length}}.", {length: "100 yards"})
           * ```
           *
           * If we were to check also against `primitiveHasOwnProperty`, as we do
           * in the dot notation case, then render call would return:
           *
           * "The length of a football field is 9."
           *
           * rather than the expected:
           *
           * "The length of a football field is 100 yards."
           **/
          lookupHit = hasProperty(context.view, name);
        }

        if (lookupHit) {
          value = intermediateValue;
          break;
        }

        context = context.parent;
      }

      cache[name] = value;
    }

    if (isFunction(value))
      value = value.call(this.view);

    return value;
  };

  /**
   * A Writer knows how to take a stream of tokens and render them to a
   * string, given a context. It also maintains a cache of templates to
   * avoid the need to parse the same template twice.
   */
  function Writer () {
    this.cache = {};
  }

  /**
   * Clears all cached templates in this writer.
   */
  Writer.prototype.clearCache = function clearCache () {
    this.cache = {};
  };

  /**
   * Parses and caches the given `template` according to the given `tags` or
   * `mustache.tags` if `tags` is omitted,  and returns the array of tokens
   * that is generated from the parse.
   */
  Writer.prototype.parse = function parse (template, tags) {
    var cache = this.cache;
    var cacheKey = template + ':' + (tags || mustache.tags).join(':');
    var tokens = cache[cacheKey];

    if (tokens == null)
      tokens = cache[cacheKey] = parseTemplate(template, tags);

    return tokens;
  };

  /**
   * High-level method that is used to render the given `template` with
   * the given `view`.
   *
   * The optional `partials` argument may be an object that contains the
   * names and templates of partials that are used in the template. It may
   * also be a function that is used to load partial templates on the fly
   * that takes a single argument: the name of the partial.
   *
   * If the optional `tags` argument is given here it must be an array with two
   * string values: the opening and closing tags used in the template (e.g.
   * [ "<%", "%>" ]). The default is to mustache.tags.
   */
  Writer.prototype.render = function render (template, view, partials, tags) {
    var tokens = this.parse(template, tags);
    var context = (view instanceof Context) ? view : new Context(view);
    return this.renderTokens(tokens, context, partials, template, tags);
  };

  /**
   * Low-level method that renders the given array of `tokens` using
   * the given `context` and `partials`.
   *
   * Note: The `originalTemplate` is only ever used to extract the portion
   * of the original template that was contained in a higher-order section.
   * If the template doesn't use higher-order sections, this argument may
   * be omitted.
   */
  Writer.prototype.renderTokens = function renderTokens (tokens, context, partials, originalTemplate, tags) {
    var buffer = '';

    var token, symbol, value;
    for (var i = 0, numTokens = tokens.length; i < numTokens; ++i) {
      value = undefined;
      token = tokens[i];
      symbol = token[0];

      if (symbol === '#') value = this.renderSection(token, context, partials, originalTemplate);
      else if (symbol === '^') value = this.renderInverted(token, context, partials, originalTemplate);
      else if (symbol === '>') value = this.renderPartial(token, context, partials, tags);
      else if (symbol === '&') value = this.unescapedValue(token, context);
      else if (symbol === 'name') value = this.escapedValue(token, context);
      else if (symbol === 'text') value = this.rawValue(token);

      if (value !== undefined)
        buffer += value;
    }

    return buffer;
  };

  Writer.prototype.renderSection = function renderSection (token, context, partials, originalTemplate) {
    var self = this;
    var buffer = '';
    var value = context.lookup(token[1]);

    // This function is used to render an arbitrary template
    // in the current context by higher-order sections.
    function subRender (template) {
      return self.render(template, context, partials);
    }

    if (!value) return;

    if (isArray(value)) {
      for (var j = 0, valueLength = value.length; j < valueLength; ++j) {
        buffer += this.renderTokens(token[4], context.push(value[j]), partials, originalTemplate);
      }
    } else if (typeof value === 'object' || typeof value === 'string' || typeof value === 'number') {
      buffer += this.renderTokens(token[4], context.push(value), partials, originalTemplate);
    } else if (isFunction(value)) {
      if (typeof originalTemplate !== 'string')
        throw new Error('Cannot use higher-order sections without the original template');

      // Extract the portion of the original template that the section contains.
      value = value.call(context.view, originalTemplate.slice(token[3], token[5]), subRender);

      if (value != null)
        buffer += value;
    } else {
      buffer += this.renderTokens(token[4], context, partials, originalTemplate);
    }
    return buffer;
  };

  Writer.prototype.renderInverted = function renderInverted (token, context, partials, originalTemplate) {
    var value = context.lookup(token[1]);

    // Use JavaScript's definition of falsy. Include empty arrays.
    // See https://github.com/janl/mustache.js/issues/186
    if (!value || (isArray(value) && value.length === 0))
      return this.renderTokens(token[4], context, partials, originalTemplate);
  };

  Writer.prototype.renderPartial = function renderPartial (token, context, partials, tags) {
    if (!partials) return;

    var value = isFunction(partials) ? partials(token[1]) : partials[token[1]];
    if (value != null)
      return this.renderTokens(this.parse(value, tags), context, partials, value);
  };

  Writer.prototype.unescapedValue = function unescapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return value;
  };

  Writer.prototype.escapedValue = function escapedValue (token, context) {
    var value = context.lookup(token[1]);
    if (value != null)
      return mustache.escape(value);
  };

  Writer.prototype.rawValue = function rawValue (token) {
    return token[1];
  };

  mustache.name = 'mustache.js';
  mustache.version = '3.0.1';
  mustache.tags = [ '{{', '}}' ];

  // All high-level mustache.* functions use this writer.
  var defaultWriter = new Writer();

  /**
   * Clears all cached templates in the default writer.
   */
  mustache.clearCache = function clearCache () {
    return defaultWriter.clearCache();
  };

  /**
   * Parses and caches the given template in the default writer and returns the
   * array of tokens it contains. Doing this ahead of time avoids the need to
   * parse templates on the fly as they are rendered.
   */
  mustache.parse = function parse (template, tags) {
    return defaultWriter.parse(template, tags);
  };

  /**
   * Renders the `template` with the given `view` and `partials` using the
   * default writer. If the optional `tags` argument is given here it must be an
   * array with two string values: the opening and closing tags used in the
   * template (e.g. [ "<%", "%>" ]). The default is to mustache.tags.
   */
  mustache.render = function render (template, view, partials, tags) {
    if (typeof template !== 'string') {
      throw new TypeError('Invalid template! Template should be a "string" ' +
                          'but "' + typeStr(template) + '" was given as the first ' +
                          'argument for mustache#render(template, view, partials)');
    }

    return defaultWriter.render(template, view, partials, tags);
  };

  // This is here for backwards compatibility with 0.4.x.,
  /*eslint-disable */ // eslint wants camel cased function name
  mustache.to_html = function to_html (template, view, partials, send) {
    /*eslint-enable*/

    var result = mustache.render(template, view, partials);

    if (isFunction(send)) {
      send(result);
    } else {
      return result;
    }
  };

  // Export the escaping function so that the user may override it.
  // See https://github.com/janl/mustache.js/issues/244
  mustache.escape = escapeHtml;

  // Export these mainly for testing, but also for advanced usage.
  mustache.Scanner = Scanner;
  mustache.Context = Context;
  mustache.Writer = Writer;

  return mustache;
}));


/***/ }),

/***/ "./src/index.ts":
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });
//import * as THREE from "three";
var crypto_utils_1 = __webpack_require__(/*! ./utils/crypto_utils */ "./src/utils/crypto_utils.ts");
var Mustache = __webpack_require__(/*! mustache */ "./node_modules/mustache/mustache.js");
;

var SelectorInterceptor = function () {
    function SelectorInterceptor(aGarden3d) {
        _classCallCheck(this, SelectorInterceptor);

        this.cid = parseInt(new Date().getTime() + "" + Math.random() * 1000000, 10);
        this.garden3dInstance = aGarden3d;
    }

    _createClass(SelectorInterceptor, [{
        key: "execute",
        value: function execute(aInterceptedObject) {
            var _this = this;

            return new Promise(function (resolve, reject) {
                try {
                    var sceneObjectRef = aInterceptedObject.object.userData.sceneObjectRef;
                    if (sceneObjectRef.isDerivedOfTypeName("ConfigurableNode")) {
                        sceneObjectRef = sceneObjectRef.findParentOfTypeName("ProductNode");
                    } else if (!sceneObjectRef.isDerivedOfTypeName("ProductNode")) {
                        throw "unaccepted node type";
                    }
                    var temporary_uid = sceneObjectRef.getValue("temporary_uid");
                    var product_uid = sceneObjectRef.getValue("object_uuid");
                    if (product_uid == null || typeof product_uid === "undefined") {
                        throw "unknown uid";
                    }
                    //if bundle selected
                    if (_this.garden3dInstance.getBundleProducts()[0].uuid === product_uid) {
                        throw "bundle selected";
                    }
                    var enableControls = false;
                    //check if this item exists in connections, if so, we can select it
                    var itemIndex = _this.garden3dInstance.getPlanConnections().findIndex(function (aItem) {
                        return aItem.configuration.temporary_uid === temporary_uid && temporary_uid !== null;
                    });
                    //console.log("found hover");
                    if (itemIndex >= 0) {
                        $("#apply_all_panel").show();
                        //resolve(aInterceptedObject);
                    } else {
                        var postIndex = _this.garden3dInstance.getPlanPosts().findIndex(function (aItem) {
                            return aItem.configuration.temporary_uid === temporary_uid && temporary_uid !== null;
                        });
                        if (postIndex < 0) {
                            //neither post or connection
                            enableControls = true;
                            $("#apply_all_panel").hide();
                        } else {
                            $("#apply_all_panel").show();
                        }
                    } //else not connection
                    $("#catalogue").hide();
                    $("#open_catalogue").removeClass("active");
                    if (!$("#open_options").hasClass("active")) {
                        $("#open_options").addClass("active");
                    }
                    $("#options").show();
                    $("#product-options").show();
                    _this.garden3dInstance.enableTransforms(enableControls);
                    if (enableControls) {
                        _this.garden3dInstance.setActiveCatalogueProduct(product_uid);
                    } else {
                        _this.garden3dInstance.setActiveCatalogueProduct(null);
                    }
                    resolve(aInterceptedObject);
                } catch (e) {
                    //nothing to select
                    console.log("not selectable: ", e);
                    reject(e);
                }
            });
        }
    }]);

    return SelectorInterceptor;
}();

var HoverInterceptor = function () {
    function HoverInterceptor(aGarden3d) {
        _classCallCheck(this, HoverInterceptor);

        this.cid = parseInt(new Date().getTime() + "" + Math.random() * 1000000, 10);
        this.garden3dInstance = aGarden3d;
    }

    _createClass(HoverInterceptor, [{
        key: "execute",
        value: function execute(aInterceptedObject) {
            try {
                var sceneObjectRef = aInterceptedObject.object.userData.sceneObjectRef;
                if (sceneObjectRef.isDerivedOfTypeName("ConfigurableNode")) {
                    sceneObjectRef = sceneObjectRef.findParentOfTypeName("ProductNode");
                } else if (!sceneObjectRef.isDerivedOfTypeName("ProductNode")) {
                    throw "unaccepted node type";
                }
                var temporary_uid = sceneObjectRef.getValue("temporary_uid");
                //check if this item exists in connections, if so, we can select it
                var itemIndex = this.garden3dInstance.getPlanConnections().findIndex(function (aItem) {
                    return aItem.configuration.temporary_uid === temporary_uid && temporary_uid !== null;
                });
                if (itemIndex >= 0) {
                    this.garden3dInstance.setHighlightedHover(aInterceptedObject.object);
                } else {
                    itemIndex = this.garden3dInstance.getPlanPosts().findIndex(function (aItem) {
                        return aItem.configuration.temporary_uid === temporary_uid && temporary_uid !== null;
                    });
                    if (itemIndex >= 0) {
                        this.garden3dInstance.setHighlightedHover(aInterceptedObject.object);
                    } else {
                        throw "not found";
                    } //else not found post
                } //else not connection
            } catch (e) {
                //nothing to select
                this.garden3dInstance.setHighlightedHover(null);
            }
            return aInterceptedObject;
        }
    }]);

    return HoverInterceptor;
}();
/** ExpiviPlanner 3d application. */


var Garden3D = function () {
    function Garden3D() {
        _classCallCheck(this, Garden3D);

        this.mPlanPosts = [];
        this.mPlanConnections = [];
        this.mBundledProducts = [];
        this.mHighlightedObject = null;
        this.mHighlightedHover = null;
        this.mCatalogueStorageKey = "xpv_garden_catalogue";
        this.mActiveCatalogueProductUUID = null;
        //mApplyingPostOptions = {attribute_name: null, attribute_value: null};
        this.mOptionChangedByUser = false;
        this.optionsTmpl = "<div class=\"catalogue container-fluid draggable-container\">\n                      <div class=\"row\">\n                      {{#products}}\n                          <div class=\"col-6\">\n                              <div class=\"product-display\" id=\"product-container\">\n                                  <section class=\"product draggable-item\" data-product=\"{{id}}\" data-price=\"{{ price }}\" data-title=\"{{ name }}\">\n                                      <a class=\"d-block content\" draggable=\"true\" data-product-id=\"{{id}}\" id=\"product_{{id}}\">\n                                          <div class=\"image-container\">\n                                            <img src=\"{{#thumbnail_url}}{{thumbnail_url}}{{/thumbnail_url}}{{^thumbnail_url}}assets/img/no-image.jpg{{/thumbnail_url}}\" class=\"img-fluid\">\n                                            {{#price}}\n                                              <div class=\"price\">&euro; {{ price }}</div>\n                                            {{/price}}\n                                          </div>\n                                          <div class=\"product-info\">\n                                              <h5 style=\"font-size: 0.8rem;color: #363741;\">{{ name }}</h5>\n                                          </div>\n                                      </a>\n                                  </section>\n                              </div>\n                          </div>\n                      {{/products}}\n                      </div>\n                  </div>";
        this.productsDropdownTemplate = "\n      <select class=\"products\">\n      {{#.}}\n        <option \n        data-fence-id=\"{{id}}\"\n        {{#configuration}}\n        value=\"{{uuid}}\"\n        {{/configuration}}\n        >\n          {{index}}: {{type}}\n        </option>\n      {{/.}}\n      </select>\n      <div class=\"optionContainer\" style=\"padding: 0\">        \n        <div id=\"apply_all_panel\" style=\"\n                margin: 10px;\n                border: 1px solid #d8e6f4;\n                padding: 5px;\n                \">\n            <div>\n            Alle schutting opties zullen mee gaan met het veranderen van onderstaand geselecteerde schutting.\n            </div>\n            <div style=\"display: flex;\n                        justify-content: center;\n                        align-items: center;\n                        padding: 5px;\n                        background-color: #ebeded;\n                        margin-top: 5px;\">\n              <span style=\"padding: 0 5px;font-size: 16px;color: #5b5762;font-weight: bold;\">\n               Op alles toepassen\n              </span>\n              \n              <label class=\"switch\">\n                <input id=\"apply_on_all\" type=\"checkbox\" checked>\n                <span class=\"slider round\"></span>\n              </label>\n            </div>\n\n        </div>\n        \n        <!--<div>Price <span data-expivi-price></span></div>-->\n        <div class=\"optionContainer\" id=\"expivi-step-container\"></div>\n        <div class=\"optionContainer\" id=\"expivi-options-container\"></div>\n      </div>\n    ";
        this.postsTemplate = "\n      {{#.}}\n        <div data-material-sku=\"{{sku}}\"\n            style=\"display: flex;\n                  flex-wrap: wrap;\">\n            {{#materials}}\n                <div data-reference-sku=\"{{sku}}\" class=\"post-material-option\">\n                  <img src=\"{{thumbnail_url}}\" width=\"50\" height=\"50\"/>\n                  <div style=\"text-overflow: ellipsis;width: 100%;overflow: hidden;\">{{name}}</div>\n                </div>\n            {{/materials}}\n        </div>\n      {{/.}}\n    ";
        this.dragElement = null;
        this.dragStartRect = new THREE.Vector4();
    }

    _createClass(Garden3D, [{
        key: "init",
        value: function init(aPlan, aDomContainerId, aToken, aDomFencePostOptions, aDomOptionsContainer) {
            try {
                $.ajaxSetup({
                    headers: {
                        'Authorization': 'Bearer ' + aToken
                    }
                });
                var now = new Date();
                //console.log("date: ", now.toISOString().slice(0, 15));
                this.mCatalogueStorageKey += "_" + this.hashCode(aToken + now.toISOString().slice(0, 15));
                //console.log("cataloge key:", this.mCatalogueStorageKey);
                this.loadPlan(aPlan);
                this.createView(aDomContainerId, aToken, aDomFencePostOptions, aDomOptionsContainer);
                this.createCatalogue();
            } catch (e) {
                console.error("Failed to read plan: ", e);
            }
        }
    }, {
        key: "hashCode",
        value: function hashCode(str) {
            return str.split('').reduce(function (prevHash, currVal) {
                return (prevHash << 5) - prevHash + currVal.charCodeAt(0) | 0;
            }, 0);
        }
    }, {
        key: "saveConfiguration",
        value: function saveConfiguration() {
            return window.expivi.saveConfiguration();
        }
    }, {
        key: "setActiveCatalogueProduct",
        value: function setActiveCatalogueProduct(aProductUUID) {
            this.mActiveCatalogueProductUUID = aProductUUID;
        }
    }, {
        key: "getPlanConnections",
        value: function getPlanConnections() {
            return this.mPlanConnections;
        }
    }, {
        key: "getPlanPosts",
        value: function getPlanPosts() {
            return this.mPlanPosts;
        }
    }, {
        key: "getBundleProducts",
        value: function getBundleProducts() {
            return this.mBundledProducts;
        }
    }, {
        key: "setHighlightedHover",
        value: function setHighlightedHover(aObject) {
            this.mHighlightedHover = typeof aObject === "undefined" ? null : aObject;
            this.updateHighlighted();
        }
    }, {
        key: "enableTransforms",
        value: function enableTransforms(aEnable) {
            var expiviViewer = window.expivi.viewer;
            var transformControls = null;
            if (expiviViewer !== null && typeof expiviViewer !== "undefined") {
                var expiviClientViewer = expiviViewer.getClientViewer();
                if (expiviClientViewer !== null && typeof expiviClientViewer !== "undefined") {
                    var expiviViewportRenderer = expiviClientViewer.getViewportRenderer();
                    if (expiviViewportRenderer !== null && typeof expiviViewportRenderer !== "undefined") {
                        transformControls = expiviViewportRenderer.getComponent('floorplanner-transform-controls');
                    }
                }
            }
            if (transformControls !== null && typeof transformControls !== "undefined") {
                if (aEnable) {
                    //console.log("transforms enabled");
                    transformControls.EnableRotationAxis(2);
                    transformControls.EnablePanning();
                    transformControls.EnableRotation();
                } else {
                    //console.log("transforms disabled");
                    transformControls.DisableRotationAxis(1 | 2 | 4);
                    transformControls.DisablePanning();
                    transformControls.DisableRotation();
                }
            }
            if (aEnable) {
                //also show remove product button
                $("#remove_product_btn").show();
            } else {
                $("#remove_product_btn").hide();
            }
        }
    }, {
        key: "setHighlightedObject",
        value: function setHighlightedObject(aObject) {
            this.mHighlightedHover = null;
            this.mHighlightedObject = typeof aObject === "undefined" ? null : aObject;
            this.updateHighlighted();
        }
    }, {
        key: "updateHighlighted",
        value: function updateHighlighted() {
            //highlight the selected object
            var renderController = window.expivi.viewer.getClientViewer().getViewportRenderer().getComponent("draw");
            if (renderController !== null) {
                var highlighted = [];
                if (this.mHighlightedObject !== null) {
                    highlighted.push(this.mHighlightedObject);
                }
                if (this.mHighlightedHover !== null && this.mHighlightedHover !== this.mHighlightedObject) {
                    highlighted.push(this.mHighlightedHover);
                }
                renderController.getOutlinePass().selectedObjects = renderController.getOutlinePass().selectedObjects || [];
                renderController.getOutlinePass().selectedObjects.forEach(function (aObject) {
                    aObject.traverse(function (aChild) {
                        if ((aChild.isMesh || aChild.isLine || aChild.isSprit) && aChild.name.indexOf('TransformControls') < 0) {
                            aChild.visible = true;
                        }
                    });
                });
                renderController.getOutlinePass().selectedObjects = highlighted;
                window.expivi._dispatch("render_frame");
            }
        }
    }, {
        key: "bindHandlers",
        value: function bindHandlers() {
            window.expivi.viewer.getClientViewer().getViewportRenderer().getComponent("selector").enableHover(true);
            window.expivi.viewer.getClientViewer().getViewportRenderer().getComponent("selector").setHoverThrothleTime(150);
            var interceptor = window.expivi.viewer.getEditor().registerInterceptor(0, new SelectorInterceptor(this));
            var hoverIntercept = window.expivi.viewer.getEditor().registerInterceptor(1, new HoverInterceptor(this));
        }
    }, {
        key: "createCatalogue",
        value: function createCatalogue() {
            var _this2 = this;

            $("#open_catalogue").click(function () {
                _this2.loadCatalogue().then(function (aProducts) {
                    var rendered = Mustache.render(_this2.optionsTmpl, { products: aProducts });
                    $("#catalogue").html(rendered);
                    $("#catalogue").on('dragstart', '[draggable="true"]', function (e) {
                        e.originalEvent.stopPropagation();
                        //console.log("setting product drag: ", $(e.currentTarget).data('product-id'));
                        e.originalEvent.dataTransfer.setData("text", JSON.stringify({
                            product_id: $(e.currentTarget).data('product-id')
                        }));
                    });
                }, function (err) {
                    $("#catalogue").html("<h3>No product found!</h3>");
                });
            });
            $("#remove_product_btn").click(function () {
                if (_this2.mActiveCatalogueProductUUID !== null) {
                    var askUser = confirm("Remove product?");
                    if (askUser) {
                        //disable transforms
                        _this2.enableTransforms(false);
                        //delete product
                        window.expivi.deleteNode(_this2.mActiveCatalogueProductUUID);
                        //select the first product in scene
                        var firstProductUUID = $("select.products option:first").val();
                        if (firstProductUUID) {
                            window.expivi.selectProductNode(firstProductUUID);
                        }
                    }
                }
            });
        }
    }, {
        key: "loadCatalogue",
        value: function loadCatalogue() {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
                //clear stored catalogue cache
                var localStorageKeys = Object.keys(sessionStorage);
                for (var i = 0; i < localStorageKeys.length; i++) {
                    if (localStorageKeys[i] !== _this3.mCatalogueStorageKey && localStorageKeys[i].slice(0, 20) === "xpv_garden_catalogue") {
                        localStorage.removeItem(localStorageKeys[i]);
                    }
                }
                //check for existing cache
                var products = [];
                var catalogueData = sessionStorage.getItem(_this3.mCatalogueStorageKey);
                if (catalogueData !== null && typeof catalogueData !== "undefined") {
                    try {
                        products = JSON.parse(catalogueData);
                        _this3.initDraggable();
                        return resolve(products);
                    } catch (e) {}
                }
                $.getJSON('https://expivi.net/api/catalogue?active=1', function (jsonData) {
                    if (jsonData && typeof jsonData.data !== "undefined") {
                        products = jsonData.data.map(function (aItem) {
                            aItem.attributes.row_width = 1; //Math.ceil(Math.random()*2);
                            return aItem.attributes;
                        });
                    }
                }).done(function () {
                    if (products.length === 0) {
                        reject("no products");
                    } else {
                        sessionStorage.setItem(_this3.mCatalogueStorageKey, JSON.stringify(products));
                        _this3.initDraggable();
                        resolve(products);
                    }
                }).fail(function () {
                    console.error("Catalogue failed to load!");
                    reject("faild to load");
                });
            });
        }
    }, {
        key: "initDraggable",
        value: function initDraggable() {
            var _this4 = this;

            if ((typeof interact === "undefined" ? "undefined" : _typeof(interact)) == undefined || interact == null) {
                console.error("Could not initialize drag-drop! InteractJS not found.");
                return;
            }
            interact('.draggable-item').draggable({
                manualStart: false,
                inertia: false,
                autoScroll: false,
                onstart: function onstart(aEvent) {
                    _this4.onItemDragStart(aEvent);
                },
                onmove: function onmove(aEvent) {
                    _this4.onItemDragged(aEvent);
                },
                onend: function onend(aEvent) {
                    _this4.onItemDragEnd(aEvent);
                }
            });
            window.expivi._events.onChange.subscribe(function (e) {
                if (e.name === "dropzone") {
                    _this4.onItemDropped(e);
                }
            });
        }
    }, {
        key: "createView",
        value: function createView(aDomContainerId, aToken, aDomFencePostOptions, aDomOptionsContainer) {
            var _this5 = this;

            //create application
            window.expivi._create(aToken, this.mBundledProducts[0].catalogue_id, //first is always default bundle product
            aDomContainerId, false, [], [], [], {
                bundle_uuid: null,
                configured_products: this.mBundledProducts
            }).then(function (aEditor) {
                //console.log("Scene is built", aEditor);
            });
            //wait until ready, and tell we are also ready
            window.expivi._events.onReady.subscribe(function () {
                //console.log("on ready!");
                window.expivi._dispatch('client_ready').then(function () {
                    //console.log("client ready done");
                    _this5.applyCuts();
                    _this5.createPostOptions(aDomFencePostOptions);
                    _this5.createOptions(aDomOptionsContainer, aToken);
                    _this5.bindHandlers();
                });
            });
        }
    }, {
        key: "applyCuts",
        value: function applyCuts() {
            this.mPlanConnections.filter(function (aConnection) {
                return aConnection.class === "FenceWall";
            }).forEach(function (aConnection) {
                if (aConnection.isDynamic === true) {
                    //apply cut on this one
                    var productUUID = aConnection.configuration.uuid;
                    window.expivi.managers.resolveMaterialGroups(productUUID).then(function (aGroups) {
                        //console.log("cut groups: ", aGroups);
                    });
                }
            });
        }
    }, {
        key: "createOptions",
        value: function createOptions(aDomOptionsContainer, aToken) {
            var _this6 = this;

            //create a list of all walls and ports
            var portsAndWalls = [];
            var indexItem = 1;
            this.mPlanConnections.forEach(function (aItem) {
                aItem.index = indexItem++;portsAndWalls.push(aItem);
            });
            //render the ports and walls dropdown
            var dropdownRender = Mustache.render(this.productsDropdownTemplate, portsAndWalls);
            //render to screen
            aDomOptionsContainer.innerHTML = dropdownRender;
            //initialize options
            //console.log("init expivi component");
            this.mOptionComponents = new ExpiviComponent.default({
                catalogueId: portsAndWalls[0].configuration.catalogue_id,
                //configuration: content,
                viewerContainer: null,
                attributesContainer: document.getElementById('expivi-options-container'),
                stepHeadContainer: document.getElementById('expivi-step-container'),
                currency: 'EUR',
                locale: 'nl',
                token: aToken,
                priceSelectors: '[data-expivi-price]'
            });
            //listen to changes on server side, select products both ways dropdown <=> view
            $("select.products").change(function (aEvent) {
                $(aEvent.currentTarget).blur();
                //console.log("item been changed: ", $(aEvent.currentTarget).val());
                $("#apply_all_panel").show();
                var selectedProductUUID = $(aEvent.currentTarget).val();
                window.expivi.selectProductNode(selectedProductUUID);
                _this6.enableTransforms(false);
                _this6.setActiveCatalogueProduct(null);
            });
            window.expivi._events.onChange.subscribe(function (e) {
                if (e.name === 'selected') {
                    var selectedItem = e.payload.mData.selection || {};
                    var userData = selectedItem.userData || {};
                    var refObject = userData.sceneObjectRef || {
                        getType: function getType() {
                            return null;
                        }
                    };
                    var refObjectType = refObject.getType();
                    if (refObjectType && refObjectType.mTypeName !== 'ProductNode') {
                        refObject = refObject.findParentOfTypeName("ProductNode");
                    }
                    refObjectType = refObject.getType();
                    if (refObjectType) {
                        $("select.products").val(refObject.mObjectUUID);
                        _this6.setHighlightedObject(selectedItem);
                    } //if we have an object uuid
                    else {
                            _this6.setHighlightedObject(null);
                        }
                } //if selected
                else if (e.name === "material" && e.payload.product_uuid === _this6.mOptionComponents.getSelectedProduct()) {
                        //console.log("material changed: ", e);
                        if ($("#apply_on_all").is(':checked')) {
                            var selectedProduct = _this6.mOptionComponents.getSelectedProduct();
                            //console.log("going to apply on all: ", selectedProduct);
                            //get the group and reference sku of selected product
                            if (selectedProduct !== null && typeof selectedProduct !== 'undefined') {
                                window.expivi.managers.resolveMaterialGroupManager(selectedProduct).then(function (aMaterialGroupManager) {
                                    var selectedMaterialGroups = aMaterialGroupManager.getGroups();
                                    var selectedGroup = selectedMaterialGroups.find(function (aGroup) {
                                        return aGroup.id === e.payload.id;
                                    });
                                    if (typeof selectedGroup !== 'undefined') {
                                        var selectedRef = selectedGroup.materials.find(function (aRef) {
                                            return aRef.id === e.payload.ref_id;
                                        });
                                        if (typeof selectedRef !== 'undefined') {
                                            //console.log("apply group on all", selectedGroup);
                                            var planItem = _this6.mPlanConnections.find(function (aWall) {
                                                return aWall.configuration.uuid === selectedProduct;
                                            }); //todo search through all selectable plan items
                                            _this6.setMaterialToSKU(_this6.mPlanConnections, selectedGroup.sku, selectedRef.sku, planItem.id);
                                        } //if found selected ref
                                    } //if found selected group
                                }, function (err) {}); //get material group manager
                            } //if has selectedProduct
                        } //if apply on all
                    } //if event is material
                    else if (e.name === "attribute") {
                            _this6.updateHeightRules(e.payload);
                        }
            });
            //select the first item already (both in scene and options)
            this.mOptionComponents.selectProduct(portsAndWalls[0].configuration.uuid);
        }
    }, {
        key: "updateHeightRules",
        value: function updateHeightRules(aEvent) {
            var _this7 = this;

            //console.log("check for option:", aEvent);
            var changedProductUUID = aEvent.product_uuid;
            if (typeof changedProductUUID !== "undefined" && typeof aEvent.attribute_name !== "undefined" && typeof aEvent.attribute_value_name !== "undefined") {
                //check if this is a door or wall
                var connectionItem = this.mPlanConnections.find(function (aItem) {
                    return aItem.configuration.uuid === changedProductUUID;
                });
                if (typeof connectionItem !== "undefined") {
                    //we need to update the begin and end of two post
                    //check if we have a mapping for question and option
                    var applyingOptionBegin = aEvent.attribute_value_name;
                    var applyingOptionEnd = aEvent.attribute_value_name;
                    var optionMaps = window.PlannerOptionMappings || {};
                    if (typeof optionMaps[aEvent.attribute_name] !== "undefined" && typeof optionMaps[aEvent.attribute_name][aEvent.attribute_value_name] !== "undefined") {
                        var optionMap = optionMaps[aEvent.attribute_name][aEvent.attribute_value_name];
                        applyingOptionBegin = optionMap.begin;
                        applyingOptionEnd = optionMap.end;
                    }
                    //change the begin
                    var beginPost = this.mPlanPosts.find(function (postItem) {
                        return postItem.id === connectionItem.start.id;
                    });
                    if (typeof beginPost !== "undefined") {
                        //apply on begin post
                        this.applyOptionOnProduct(beginPost.configuration.uuid, aEvent.attribute_name, applyingOptionBegin);
                    } //if begin post
                    //change end post
                    var endPost = this.mPlanPosts.find(function (postItem) {
                        return postItem.id === connectionItem.end.id;
                    });
                    if (typeof endPost !== "undefined") {
                        //apply on end post
                        this.applyOptionOnProduct(endPost.configuration.uuid, aEvent.attribute_name, applyingOptionEnd);
                    } //if end post
                } //if connectionItem
                else if ($("#apply_on_all").is(':checked') && (!aEvent.user_data || aEvent.user_data.forced_attribute !== true)) {
                        //check if the event is one of the post
                        var changedPost = this.mPlanPosts.find(function (postItem) {
                            return postItem.configuration && postItem.configuration.uuid === changedProductUUID;
                        });
                        if (typeof changedPost !== "undefined") {
                            this.mPlanPosts.forEach(function (postItem) {
                                if (postItem.id === changedPost.id || postItem.configuration === null || typeof postItem.configuration === 'undefined') return;
                                _this7.applyOptionOnProduct(postItem.configuration.uuid, aEvent.attribute_name, aEvent.attribute_value_name);
                            });
                        }
                    } //else not connection
            } //if product uuid
        }
    }, {
        key: "applyOptionOnProduct",
        value: function applyOptionOnProduct(aProductUUID, aQuestionName, aOptionName) {
            if (aProductUUID === null || typeof aProductUUID === "undefined") return;
            //get the product options (to find out the option_id of name)
            window.expivi.getProductAttributes(aProductUUID).then(function (aProductAttributes) {
                //console.log("resolve begin post product options:", aProductAttributes);
                if (aProductAttributes !== null && typeof aProductAttributes !== "undefined") {
                    //find the question
                    var questionToSelect = null;
                    aProductAttributes.every(function (aOptionItem) {
                        if (aOptionItem.name === aQuestionName && aOptionItem.type === "question") {
                            questionToSelect = aOptionItem;
                            return false;
                        }
                        return true;
                    });
                    if (questionToSelect === null) return;
                    //find the option
                    var optionToSelect = null;
                    if (questionToSelect.attribute_values === null || typeof questionToSelect.attribute_values === "undefined") return;
                    questionToSelect.attribute_values.every(function (attributeValue) {
                        if (attributeValue.name === aOptionName) {
                            optionToSelect = attributeValue;
                            return false;
                        }
                        return true;
                    });
                    if (optionToSelect === null) return;
                    //apply
                    window.expivi.setProductAttribute(questionToSelect.id, optionToSelect.id, aProductUUID, {
                        forced_attribute: true
                    });
                }
            });
        }
    }, {
        key: "createPostOptions",
        value: function createPostOptions(aDomPostOptions) {
            var _this8 = this;

            //console.log("create post options");
            if (this.mPlanPosts.length === 0) return;
            //resolve first product
            var productUUID = this.mPlanPosts[0].configuration.uuid;
            window.expivi.managers.resolveMaterialGroupManager(productUUID).then(function (aMaterialGroupManager) {
                var mainMaterialGroups = aMaterialGroupManager.getGroups();
                //render the materials
                _this8.renderPostTemplate(aDomPostOptions, mainMaterialGroups);
                //get group information and apply on all other products
                mainMaterialGroups.forEach(function (aGroup) {
                    _this8.setMaterialToSKU(_this8.mPlanPosts, aGroup.sku, aMaterialGroupManager.getActiveRefModel(aGroup.id).sku, _this8.mPlanPosts[0].id);
                });
            }, function (err) {
                console.error("Failed to get material group manager");
            });
        }
    }, {
        key: "renderPostTemplate",
        value: function renderPostTemplate(aDomPostOptions, mainMaterialGroups) {
            var _this9 = this;

            var rendered = Mustache.render(this.postsTemplate, mainMaterialGroups);
            //console.log("render post template");
            aDomPostOptions.innerHTML = rendered;
            $("[data-reference-sku]").click(function (aEvent) {
                var selectedDom = $(aEvent.currentTarget);
                var parentGroup = selectedDom.closest("[data-material-sku]");
                //console.log(selectedDom.data("reference-sku"), parentGroup.data("material-sku"));
                _this9.setMaterialToSKU(_this9.mPlanPosts, parentGroup.data("material-sku"), selectedDom.data("reference-sku"));
            });
        }
    }, {
        key: "setMaterialToSKU",
        value: function setMaterialToSKU(aItems, aGroupSKU, aReferenceSKU) {
            var aSkipId = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

            aItems.filter(function (aPost) {
                return (aPost.class === "FenceWall" || aPost.class === "FencePost") && (aSkipId === null || aPost.id !== aSkipId);
            }).forEach(function (aPost) {
                window.expivi.managers.resolveMaterialGroupManager(aPost.configuration.uuid).then(function (aMaterialGroupManager) {
                    var materialGroups = aMaterialGroupManager.getGroups();
                    var changedGroup = materialGroups.find(function (aGroup) {
                        return aGroup.sku === aGroupSKU;
                    });
                    //console.log("changed group: ", changedGroup);
                    if (typeof changedGroup !== "undefined") {
                        var appliedReference = changedGroup.materials.find(function (aReference) {
                            return aReference.sku === aReferenceSKU;
                        });
                        //console.log("changed reference: ", appliedReference);
                        if (typeof appliedReference !== "undefined") {
                            aMaterialGroupManager.setActiveMaterial(changedGroup.id, appliedReference.id);
                        } //reference sku exists
                    } //group sku exists
                }); //resolve material manager of post
            }); //foreach post
        }
    }, {
        key: "loadPlan",
        value: function loadPlan(aPlan) {
            //console.log("Load with plan: ", aPlan);
            //reset to new plan
            this.mPlan = aPlan;
            //read and [re]set plan data
            this.readPlan();
            //init default configurations
            this.initDefaultConfigurations();
        }
    }, {
        key: "readPlan",
        value: function readPlan() {
            var _this10 = this;

            var planner = this.mPlan.planner;
            var pixels2Meters = planner.meter2pixel;
            //console.log("meters to pixels: " + pixels2Meters);
            var fences = planner.fences;
            this.mPlanPosts = [];
            this.mPlanConnections = [];
            var bbminx = Infinity;
            var bbmaxx = -Infinity;
            var bbminy = Infinity;
            var bbmaxy = -Infinity;
            fences.forEach(function (item) {
                if (item.class === "FenceWall" || item.class === "FenceGate") {
                    var beginPostId = item.start.id;
                    var endPostId = item.end.id;
                    if (typeof beginPostId === "undefined" || typeof endPostId === "undefined") {
                        throw "connection points misses begin or end";
                    }
                    //resolve begin post
                    var beginPost = _this10.mPlanPosts.find(function (postItem) {
                        return postItem.id === beginPostId;
                    });
                    if (typeof beginPost === "undefined") {
                        var newPost = fences.find(function (postItem) {
                            return postItem.class === "FencePost" && postItem.id === beginPostId;
                        });
                        if (typeof newPost === "undefined") {
                            throw "Could not find post" + beginPostId;
                        }
                        beginPost = newPost;
                        _this10.mPlanPosts.push(beginPost);
                    }
                    //resolve end post
                    var endPost = _this10.mPlanPosts.find(function (postItem) {
                        return postItem.id === endPostId;
                    });
                    if (typeof endPost === "undefined") {
                        var _newPost = fences.find(function (postItem) {
                            return postItem.class === "FencePost" && postItem.id === endPostId;
                        });
                        if (typeof _newPost === "undefined") {
                            throw "Could not find post" + endPostId;
                        }
                        endPost = _newPost;
                        _this10.mPlanPosts.push(endPost);
                    }
                    //connection position
                    var connectionPosition = new THREE.Vector2(beginPost.position.x, beginPost.position.y);
                    var endPosition = new THREE.Vector2(endPost.position.x, endPost.position.y);
                    //bounding box
                    bbminx = bbminx > connectionPosition.x ? connectionPosition.x : bbminx;
                    bbminx = bbminx > endPosition.x ? endPosition.x : bbminx;
                    bbmaxx = bbmaxx < connectionPosition.x ? connectionPosition.x : bbmaxx;
                    bbmaxx = bbmaxx < endPosition.x ? endPosition.x : bbmaxx;
                    bbminy = bbminy > connectionPosition.y ? connectionPosition.y : bbminy;
                    bbminy = bbminy > endPosition.y ? endPosition.y : bbminy;
                    bbmaxy = bbmaxy < connectionPosition.y ? connectionPosition.y : bbmaxy;
                    bbmaxy = bbmaxy < endPosition.y ? endPosition.y : bbmaxy;
                    item.beginPosition = connectionPosition;
                    item.endPosition = endPosition;
                    item.postOffset = (beginPost.length * 0.5 + beginPost.offset) / 100; //in centimeters
                    if (item.class === "FenceWall" || item.class === "FenceGate") {
                        _this10.mPlanConnections.push(item);
                    }
                }
            });
            var minVec = new THREE.Vector2(bbminx, bbminy);
            var maxVec = new THREE.Vector2(bbmaxx, bbmaxy);
            var bbSizeVec = maxVec.clone().sub(minVec);
            //console.log("begin point:", minVec.toArray());
            //console.log("end point:", maxVec.toArray(), bbSizeVec.y);
            if (bbSizeVec.lengthSq() === 0) throw "zero sized plan";
            this.mPlanPosts.forEach(function (postItem) {
                //reposition origin
                var position = new THREE.Vector2(postItem.position.x - bbminx - bbSizeVec.x * 0.5, bbmaxy - postItem.position.y - bbSizeVec.y * 0.5);
                //console.log(":",position.toArray());
                //convert to meters
                postItem.position2 = position.divideScalar(pixels2Meters); //.divide(bbSizeVec).multiplyScalar(pixels2Units);
                //console.log(" position:", postItem.position2.toArray());
            });
            this.mPlanConnections.forEach(function (connectionItem) {
                //reposition origin
                connectionItem.position2 = new THREE.Vector2(connectionItem.beginPosition.x - bbminx - bbSizeVec.x * 0.5, bbmaxy - connectionItem.beginPosition.y - bbSizeVec.y * 0.5);
                var wallEndPosition = new THREE.Vector2(connectionItem.endPosition.x - bbminx - bbSizeVec.x * 0.5, bbmaxy - connectionItem.endPosition.y - bbSizeVec.y * 0.5);
                //convert to meters
                connectionItem.position2 = connectionItem.position2.divideScalar(pixels2Meters); //.divide(bbSizeVec).multiplyScalar(pixels2Units);
                wallEndPosition.divideScalar(pixels2Meters);
                connectionItem.endPosition2 = wallEndPosition.clone();
                //direction vector
                connectionItem.directionVec = wallEndPosition.clone().sub(connectionItem.position2);
                //add post offset to position
                connectionItem.position2.add(connectionItem.directionVec.clone().normalize().multiplyScalar(connectionItem.postOffset));
                //console.log("offset:", wallItem.directionVec.clone().normalize().multiplyScalar(wallItem.postOffset).toArray(), wallItem.postOffset);
                //console.log("position:", wallItem.position2.toArray());
                //console.log("direction:", wallItem.directionVec.toArray());
            });
        }
    }, {
        key: "setupConfigurationObject",
        value: function setupConfigurationObject(aItem, aBundleId, aBundledProductId, aDefaultViewId) {
            //load the default configuration for file type
            var productDefaultConfiguration = PlannerDefaultConfigurations.find(function (aDefConf) {
                return aDefConf.type === aItem.type;
            });
            if (typeof productDefaultConfiguration === 'undefined') {
                aItem.configuration = null;
                throw "Could not find the default configuration of type: " + aItem.type;
            }
            aItem.configuration = aItem.meta || null;
            if (aItem.configuration === null || typeof aItem.configuration.version === 'undefined') {
                //duplicate
                aItem.configuration = {};
                Object.keys(productDefaultConfiguration).forEach(function (aKey) {
                    if (aKey !== "place_holder") {
                        //dont duplicate functions
                        aItem.configuration[aKey] = JSON.parse(JSON.stringify(productDefaultConfiguration[aKey]));
                    } else if (productDefaultConfiguration[aKey] !== null && typeof productDefaultConfiguration[aKey] !== "undefined") {
                        aItem.configuration[aKey] = productDefaultConfiguration[aKey];
                    }
                });
                aItem.configuration.uuid = crypto_utils_1.CryptoUtils.guid();
                //console.log("setup configuration: ", aItem.configuration);
            } else if (typeof productDefaultConfiguration.place_holder !== "undefined") {
                aItem.configuration.place_holder = productDefaultConfiguration.place_holder;
            }
            //set temporary uid to identify product
            aItem.configuration.temporary_uid = aItem.id;
            aItem.configuration.parent_uuid = aBundledProductId;
            aItem.configuration.bundle_uuid = aBundleId;
            aItem.configuration.view_id = aDefaultViewId;
            aItem.configuration.view_position = [aItem.position2.x, 0, -aItem.position2.y];
            if (aItem.class === "FenceWall" || aItem.class === "FenceGate") {
                var directionVec = aItem.directionVec.clone().normalize();
                var directionAngle = directionVec.angle() - 1.5708;
                aItem.configuration.view_rotation = [0, directionAngle, 0];
            }
            if (aItem.isDynamic === true) {
                var beginPosition = new THREE.Vector3(aItem.position2.x, 0, -aItem.position2.y);
                var endPosition = new THREE.Vector3(aItem.endPosition2.x, 0, -aItem.endPosition2.y);
                var direction = endPosition.clone().sub(beginPosition).normalize();
                //let planeTarget = beginPosition.clone().addScaledVector(direction.normalize(), aItem.length / this.mPlan.planner.meter2pixel);
                //let planeDirection = planeTarget.clone().normalize();
                //planeDirection.negate();
                aItem.configuration.clipping_plane = JSON.stringify({
                    normal: [-direction.x, 0, -direction.z],
                    point: [endPosition.x, 0, endPosition.z]
                });
                //console.log("made clipping: ", aItem.configuration.clipping_plane);
            }
        }
    }, {
        key: "initDefaultConfigurations",
        value: function initDefaultConfigurations() {
            var _this11 = this;

            //console.log("init default configurations");
            //init bundle
            var bundleId = crypto_utils_1.CryptoUtils.guid();
            var bundledProductId = crypto_utils_1.CryptoUtils.guid();
            var bundledProductConfiguration = JSON.parse(JSON.stringify( //duplicate
            PlannerDefaultConfigurations.find(function (aDefConf) {
                return aDefConf.type === "bundle";
            })));
            bundledProductConfiguration.uuid = bundledProductId;
            bundledProductConfiguration.bundle_uuid = bundleId;
            var bundleViewId = bundledProductConfiguration.bundle_viewid;
            //new bundled array
            this.mBundledProducts = [];
            this.mBundledProducts.push(bundledProductConfiguration);
            //put each item as a product inside bundle
            this.mPlanPosts.forEach(function (aPost) {
                _this11.setupConfigurationObject(aPost, bundleId, bundledProductId, bundleViewId);
                _this11.mBundledProducts.push(aPost.configuration);
            });
            this.mPlanConnections.forEach(function (aConnection) {
                _this11.setupConfigurationObject(aConnection, bundleId, bundledProductId, bundleViewId);
                _this11.mBundledProducts.push(aConnection.configuration);
            });
        }
        // -- Dragging ---

    }, {
        key: "onItemDragStart",
        value: function onItemDragStart(aEvent) {
            var target = aEvent.currentTarget;
            var x = aEvent.clientX;
            var y = aEvent.clientY;
            if (this.dragElement != null) {
                document.body.removeChild(this.dragElement);
                this.dragElement = null;
            }
            this.dragElement = target.cloneNode(true); // deep-clone
            document.body.appendChild(this.dragElement);
            this.dragElement.style.position = 'absolute';
            this.dragElement.style.top = this.dragElement.style.left = "0px";
            this.dragElement.style.zIndex = '99999';
            this.dragElement.style.transform = "translate(" + x + "px, " + y + "px) scale(1, 1)";
            this.dragElement.classList.add("dragging-element-active");
            var rect = target.getBoundingClientRect();
            this.dragStartRect.set(rect.x, rect.y, rect.width, rect.height);
            window.expivi._dispatch("drag", { type: "start", event: aEvent });
        }
    }, {
        key: "onItemDragged",
        value: function onItemDragged(aEvent) {
            var target = aEvent.currentTarget;
            var x = aEvent.clientX;
            var y = aEvent.clientY;
            if (this.dragElement) {
                this.dragElement.style.transform = "translate(" + x + "px, " + y + "px) scale(1, 1)";
            }
            window.expivi._dispatch("drag", { type: "moved", event: aEvent });
        }
    }, {
        key: "onItemDragEnd",
        value: function onItemDragEnd(aEvent) {
            var target = aEvent.currentTarget;
            var x = aEvent.clientX;
            var y = aEvent.clientY;
            window.expivi._dispatch("drag", { type: "end", event: aEvent });
        }
    }, {
        key: "onItemDropped",
        value: function onItemDropped(aEvent) {
            var _this12 = this;

            var target = aEvent.payload.data.target;
            var x = aEvent.payload.data.event.clientX;
            var y = aEvent.payload.data.event.clientY;
            var didDrop = aEvent.payload.dropped;
            if (didDrop) {
                // Drop animation?
                //console.log("Dropzone found! Dropping element..");
                this.dragElement.style.transition = "transform 0.3s linear 0s";
                var transform = "translate(" + x + "px, " + y + "px) scale(0, 0)";
                this.dragElement.style.transform = transform;
                setTimeout(function () {
                    if (_this12.dragElement) {
                        document.body.removeChild(_this12.dragElement);
                        _this12.dragElement = null;
                    }
                }, 400);
            } else {
                // Translating back
                //console.log("Didn't find dropzone! Translating back..");
                this.dragElement.style.transition = "transform 0.1s linear 0s" + ", width 0.1s linear 0s" + ", height 0.1s linear 0s";
                var _transform = "translate(" + this.dragStartRect.x + "px, " + this.dragStartRect.y + "px) scale(1, 1)";
                this.dragElement.style.transform = _transform;
                this.dragElement.style.width = this.dragStartRect.z + "px";
                this.dragElement.style.height = this.dragStartRect.w + "px";
                setTimeout(function () {
                    if (_this12.dragElement) {
                        document.body.removeChild(_this12.dragElement);
                        _this12.dragElement = null;
                    }
                }, 200);
            }
        }
    }]);

    return Garden3D;
}();

exports.Garden3D = Garden3D;

/***/ }),

/***/ "./src/utils/crypto_utils.ts":
/*!***********************************!*\
  !*** ./src/utils/crypto_utils.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

Object.defineProperty(exports, "__esModule", { value: true });

var CryptoUtils = function () {
    _createClass(CryptoUtils, null, [{
        key: "guid",
        value: function guid() {
            function s4() {
                return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
            }
            ;
            return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
        }
    }, {
        key: "isUUID",
        value: function isUUID(aValue) {
            return aValue !== null && typeof aValue !== 'undefined' && /(.*)\-(.*)\-(.*)/.test(aValue);
        }
    }]);

    function CryptoUtils() {
        _classCallCheck(this, CryptoUtils);
    }

    return CryptoUtils;
}();

exports.CryptoUtils = CryptoUtils;

/***/ }),

/***/ 0:
/*!****************************!*\
  !*** multi ./src/index.ts ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./src/index.ts */"./src/index.ts");


/***/ })

/******/ });
});
//# sourceMappingURL=3d-fence-planner.bundle.js.map