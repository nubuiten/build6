var menuToggle;
var menuToggleArrow;
var sidemenu, containerTop;
var fenceOptionsContainer;
var fencePostsContainer;
var optionsContainer;
var catalogueContainer;
var isMenuOpen = true;
var planner;

var btnOpenOption;
var btnOpenCatalogue;
var saveOverlay;

//var temp_plan = {"id":"4A94479C-2F4F-44A3-A046-9B0F5EA32E03","name":"GardenPlanner","version":"0.0.1","author":"Expivi","planner":{"width-left":5,"width-right":10,"length-top":4,"length-bottom":10,"shape":"l-shape-right-up","meter2pixel":87.95238385881696,"origin":{"x":166.3931479988281,"y":-79.97726344835928},"centerScreen":{"x":1127,"y":647.6666870117188},"fences":[{"id":"6E683F73-E407-43F9-AA23-2A3F89B24683","class":"FencePost","position":{"x":846,"y":401},"start":["586FFCA5-4AF6-47B3-85B5-D1F56D8A0B70"],"end":[],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"586FFCA5-4AF6-47B3-85B5-D1F56D8A0B70","class":"FenceWall","start":{"id":"6E683F73-E407-43F9-AA23-2A3F89B24683"},"end":{"id":"42E8DC5D-9F8A-4791-8C9F-714AD93CA0B6"},"length":90,"type":"schutting4","isDynamic":false},{"id":"42E8DC5D-9F8A-4791-8C9F-714AD93CA0B6","class":"FencePost","position":{"x":931.3138123430525,"y":401},"start":["8EA3FA9B-0854-4461-8385-383BCB8E103C"],"end":["586FFCA5-4AF6-47B3-85B5-D1F56D8A0B70"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"8EA3FA9B-0854-4461-8385-383BCB8E103C","class":"FenceWall","start":{"id":"42E8DC5D-9F8A-4791-8C9F-714AD93CA0B6"},"end":{"id":"3A06E423-0FFB-4D43-A08A-5D3051A97DDB"},"length":90,"type":"schutting4","isDynamic":false},{"id":"3A06E423-0FFB-4D43-A08A-5D3051A97DDB","class":"FencePost","position":{"x":1016.6276246861049,"y":401},"start":["9CFB7470-10EB-4A39-A26B-EB98515DC109"],"end":["8EA3FA9B-0854-4461-8385-383BCB8E103C"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"9CFB7470-10EB-4A39-A26B-EB98515DC109","class":"FenceWall","start":{"id":"3A06E423-0FFB-4D43-A08A-5D3051A97DDB"},"end":{"id":"CD003A10-275C-40E6-A86A-E1C2ADE05472"},"length":90,"type":"schutting4","isDynamic":false},{"id":"CD003A10-275C-40E6-A86A-E1C2ADE05472","class":"FencePost","position":{"x":1101.9414370291572,"y":401},"start":["8FDAC546-7932-4B24-88E7-A0A6CDAA05E5"],"end":["9CFB7470-10EB-4A39-A26B-EB98515DC109"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"8FDAC546-7932-4B24-88E7-A0A6CDAA05E5","class":"FenceWall","start":{"id":"CD003A10-275C-40E6-A86A-E1C2ADE05472"},"end":{"id":"42CAAF3B-691A-4967-8836-D938AEBA8A08"},"length":90,"type":"schutting4","isDynamic":false},{"id":"42CAAF3B-691A-4967-8836-D938AEBA8A08","class":"FencePost","position":{"x":1101.9414370291572,"y":486.31381234305246},"start":["175C4B80-48B2-46E3-8563-80C031F3B8BB"],"end":["8FDAC546-7932-4B24-88E7-A0A6CDAA05E5"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"175C4B80-48B2-46E3-8563-80C031F3B8BB","class":"FenceWall","start":{"id":"42CAAF3B-691A-4967-8836-D938AEBA8A08"},"end":{"id":"04611465-8C3E-449A-9C38-72B1910395E3"},"length":90,"type":"schutting4","isDynamic":false},{"id":"04611465-8C3E-449A-9C38-72B1910395E3","class":"FencePost","position":{"x":1101.9414370291572,"y":571.6276246861049},"start":["06E1B5EF-79BB-46A7-866B-C0ABBDB543D3"],"end":["175C4B80-48B2-46E3-8563-80C031F3B8BB"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"06E1B5EF-79BB-46A7-866B-C0ABBDB543D3","class":"FenceWall","start":{"id":"04611465-8C3E-449A-9C38-72B1910395E3"},"end":{"id":"5DF3EC68-30E4-4BBE-94AB-CA3CF5FF8F78"},"length":90,"type":"schutting4","isDynamic":false},{"id":"5DF3EC68-30E4-4BBE-94AB-CA3CF5FF8F78","class":"FencePost","position":{"x":1101.9414370291572,"y":656.9414370291573},"start":["341BFCBB-741B-4647-B1C4-EFE5016AE3A3"],"end":["06E1B5EF-79BB-46A7-866B-C0ABBDB543D3"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"341BFCBB-741B-4647-B1C4-EFE5016AE3A3","class":"FenceWall","start":{"id":"5DF3EC68-30E4-4BBE-94AB-CA3CF5FF8F78"},"end":{"id":"0BBD89CF-A58C-4F02-B411-D97747222356"},"length":90,"type":"schutting4","isDynamic":false},{"id":"0BBD89CF-A58C-4F02-B411-D97747222356","class":"FencePost","position":{"x":1101.9414370291572,"y":742.2552493722097},"start":["50F6F276-04F9-4DFE-B63B-E81359F72741"],"end":["341BFCBB-741B-4647-B1C4-EFE5016AE3A3"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"50F6F276-04F9-4DFE-B63B-E81359F72741","class":"FenceWall","start":{"id":"0BBD89CF-A58C-4F02-B411-D97747222356"},"end":{"id":"DFE3E2B1-A3E3-45B2-A99C-65779156F18D"},"length":90,"type":"schutting4","isDynamic":false},{"id":"DFE3E2B1-A3E3-45B2-A99C-65779156F18D","class":"FencePost","position":{"x":1016.6276246861047,"y":742.2552493722097},"start":["6B922939-0CEB-4ADC-9E67-A54A5147433F"],"end":["50F6F276-04F9-4DFE-B63B-E81359F72741"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"6B922939-0CEB-4ADC-9E67-A54A5147433F","class":"FenceWall","start":{"id":"DFE3E2B1-A3E3-45B2-A99C-65779156F18D"},"end":{"id":"4F1A68D3-9A35-4032-B832-56BDFAF27EF2"},"length":90,"type":"schutting4","isDynamic":false},{"id":"4F1A68D3-9A35-4032-B832-56BDFAF27EF2","class":"FencePost","position":{"x":931.3138123430523,"y":742.2552493722097},"start":["377E8B48-B99B-4FB0-A9BD-BC2C4BE80EBA"],"end":["6B922939-0CEB-4ADC-9E67-A54A5147433F"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0},{"id":"377E8B48-B99B-4FB0-A9BD-BC2C4BE80EBA","class":"FenceWall","start":{"id":"4F1A68D3-9A35-4032-B832-56BDFAF27EF2"},"end":{"id":"0B94E47B-82CC-4878-82ED-35A2911C6993"},"length":90,"type":"schutting4","isDynamic":false},{"id":"0B94E47B-82CC-4878-82ED-35A2911C6993","class":"FencePost","position":{"x":845.9999999999999,"y":742.2552493722097},"start":[],"end":["377E8B48-B99B-4FB0-A9BD-BC2C4BE80EBA"],"length":7,"type":"fijnbezaagde hardhouten paal","offset":0}]}};
//var expivi_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImY1MDNlZjA5OTNhMTlhN2I2YTc2ZTViYTAwZTMzNTIyMDBhZjhjZmViN2UxOWQxNzJhNzUwOWZlNWQ2ZDhlMTYzMjhlNTkyNDA5ZmM3NjA2In0.eyJhdWQiOiIxIiwianRpIjoiZjUwM2VmMDk5M2ExOWE3YjZhNzZlNWJhMDBlMzM1MjIwMGFmOGNmZWI3ZTE5ZDE3MmE3NTA5ZmU1ZDZkOGUxNjMyOGU1OTI0MDlmYzc2MDYiLCJpYXQiOjE1NDc3MTUwNTQsIm5iZiI6MTU0NzcxNTA1NCwiZXhwIjoxNTc5MjUxMDU0LCJzdWIiOiIzIiwic2NvcGVzIjpbInJlYWQiXX0.UGHVW9ionyDZ8spFMVh4uF6qVDjtrNyGydrPNAHjlupp6sfe0sa7pi7NQGPT9MdJMHbHWFfW3qgbf5oTeQIQKnF-4PO0f1D4FGZz-I-8Em0RIMhKsN9SgZAKeyVmozs-uXVNxfkE8uRQGZ3nploof0xsL7KodCO1Yx6XsC5H2GvLZx3R36LP00CNuWbeZcL18wMatPDUFjfCCzL-07idHuVD1TFUnOa48rB_UkpFuFJATdc367PKtX2g_O30N4cgzDmqehdTjfEqFPgchScl9pNbPg_JLMjD3_ENW3a1FJcXx-OFKNZ_slwPr-R8Fm12e2zkWEsiJsdZx3-kaNmRWqKor0ciCkHj-RnHdt2auJdkWOpVYW83SCzs1Q374r6L6lZS2oBCDse8asedeYopOiE6zw-tjs4dLcwJ6QyQYWq9_GfUv52lfX4CUXO9l5njv6BouYIsLFzt0Un39S45nuVDWRHTxPYVvtNdxGWc27QSwMJZPhLsevjfDw3NNSvYo1WG91OvK0PkxPbk84iZv69sr9tF34Lg7tHMQY1j1J7zxtJretNg-oyO-ny1PEziCsNtM0D0njpeoZ_ZLkqBrE0hTk4Mp2LHOmA0041ahFQYaSl5bjcFbYrPxluxPY0pcaBzoUDyNKAOB8z86Om_-nLvuxX22Y5qSF4o4fX09rI";
var nubuiten_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImU1MTFiZDFlMGNiMGRjNmMxZjk1NmM4MjAyMjljNzgxMWQxZjZkZGQ5ZDQwM2U0ZjlkOGVlMTA4NmI1NmUzNTcyMjE5MzAzZWQ5MmVmNWEzIn0.eyJhdWQiOiIxIiwianRpIjoiZTUxMWJkMWUwY2IwZGM2YzFmOTU2YzgyMDIyOWM3ODExZDFmNmRkZDlkNDAzZTRmOWQ4ZWUxMDg2YjU2ZTM1NzIyMTkzMDNlZDkyZWY1YTMiLCJpYXQiOjE1NDg3ODU0NDQsIm5iZiI6MTU0ODc4NTQ0NCwiZXhwIjoxNTgwMzIxNDQ0LCJzdWIiOiI1MSIsInNjb3BlcyI6WyJyZWFkIl19.P7-tDhmyK6bIi1GOAsJyeEH2fj9INQGVsBRqL2orXGAhGAf17C8OuCOmcFsw5sj_ldg9rYez1RrQMlQEKCoKesUFTqIqIrUJ-OLVXZ58uUq6PrlKwBCI_g-xj1nP9Y_PbH1Ukdjhiyq8mQClvfddLWclSggBOOYJzwBEMBJajp8WtRE96B2zKVAoYJuGPKT-rL5hEW1NsQOF2snaO_ECb1-hX1FINvrtdtb-R0Zm-vN6XQsFpAVcMx9XLMX7_O3mEvY5o6jiyZw1aYMYgja35rAAmPS_3vgDJ8OVLUWrHE7n0ebbP-G4VBwDEDz8tyaWqv0GEetCwKklw4XH6byP4HKd7aqmnuwv4xaYXMq7IQAI8Iz5lBYkax8J_oiyuOxW6Zs0KszDMrdcjbX5ot_alNLignGkOkwv06dQBg23iURXNPRvywdEVhLJs8lk_3n6fv6Fj0G08wqYcP9THkfM-pIrd12DM7l0TWxrhvsO1xMG3ZmEsYPtFwjsSam7TYsJ86L_-ZYBj4-Hwg9XnwMPSdemaqdMpdPdsECWNP98-eLa0wQip49xPglWjx2frJLR2zAexjpAEjrniJhZ-Ya6StuIYrl5mwBJzi7TohCNsX4eIKBAJ3RHppgoJym9G0usPn5xrmjSPjSPq4UDCrbkhKORCmIyJMhAQ2HKlRqUCsI";
function init()
{
    menuToggle = document.getElementById("close_btn");
    menuToggleArrow = document.getElementById("toggleArrow");
    
    sidemenu = document.getElementById("sidebar-left");
    containerTop = document.getElementById("container-top");

    btnOpenOption = document.getElementById("open_options");
    btnOpenCatalogue = document.getElementById("open_catalogue");

    fenceOptionsContainer = document.getElementById("fenceOptionsItems");
    fencePostsContainer = document.getElementById("fencepostOptionItems");
    catalogueContainer = document.getElementById("catalogue");
    optionsContainer = document.getElementById("options");
    saveOverlay = document.getElementById("saveOverlay");

    menuToggle.onclick = () => {
        if (isMenuOpen) {
            sidemenu.classList.add("closed");
            menuToggle.classList.add("closed");
            menuToggleArrow.classList.add("closed");
            containerTop.classList.add("closed");
        } else {
            sidemenu.classList.remove("closed");
            menuToggle.classList.remove("closed");
            menuToggleArrow.classList.remove("closed");
            containerTop.classList.remove("closed");
        }
        isMenuOpen = !isMenuOpen;
    };

    planner = new Garden3D();
    try{
        var plan = JSON.parse(localStorage.getItem("planner"));
        planner.init(plan, "#canvas-container", nubuiten_token, fencePostsContainer, fenceOptionsContainer);
    }catch(e){
    }

    btnOpenOption.onclick = ()=>{
        $(optionsContainer).show();
        $(catalogueContainer).hide();
        $("#product-options").show();
        btnOpenCatalogue.classList.remove("active");
        btnOpenOption.classList.add("active");
    }

    btnOpenCatalogue.onclick = ()=>{
        $(optionsContainer).hide();
        $(catalogueContainer).show();
        $("#product-options").hide();
        btnOpenCatalogue.classList.add("active");
        btnOpenOption.classList.remove("active");
    }

    var btnPlanStep = document.getElementById("planStep");
    btnPlanStep.onclick = (ev)=>{
        if (ev) ev.preventDefault();
        console.log("Clicked step");

        saveOverlay.style.display = "flex";

        planner.saveConfiguration().then(function(aConfiguredBundle){
            saveOverlay.style.display = "none";
            try{
                let configuredProducts = aConfiguredBundle.configured_proudcts;
                let prevConf = JSON.parse(localStorage.getItem("planner"));
                let prevPlan = prevConf.planner.fences;
                for(var i=0; i < configuredProducts.length; i++){
                    for(var j=0; j < prevPlan.length; j++){
                        if(prevPlan[j].id === configuredProducts[i].temporary_uid){
                            prevPlan[j].meta = configuredProducts[i];
                            break;
                        }
                    }
                }
                localStorage.setItem("planner", JSON.stringify(prevConf));
                console.log("saved planner:", JSON.stringify(prevConf));
            }catch(e){}
            window.location.assign("./index.html"); // redirect to planner page
        }, (error)=>{
            //saveOverlay.style.display = "none";
            console.log("Failed to save configuration: ", error);
            window.location.assign("./index.html"); // redirect to planner page
        });
    }
}

window.onload = init;
