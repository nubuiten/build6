(function(aNamespace){
  var animatedTexture = new THREE.TextureLoader().load( 'assets/img/animated_uv.png' );
  animatedTexture.wrapS = THREE.RepeatWrapping;
  animatedTexture.wrapT = THREE.RepeatWrapping;
  var animatedTextureUvPosition = 0;
  var animatedTextureTimeout = null;

  function animateTextureUVPosition(){
    if(animatedTextureTimeout === null){
      animatedTextureTimeout = setTimeout(function(){
        animatedTextureTimeout = null;
        animatedTextureUvPosition += 0.01;
        animatedTexture.offset.set(animatedTextureUvPosition, animatedTextureUvPosition*0.5);
      }, 32);
    }
  }

  aNamespace.PlannerDefaultConfigurations = [
    {
      "version": "2.0",
      "type": "bundle",
      "catalogue_id": "707",
      "configuration": {
        "materials": {},
        "attributes": []
      },
      "attributes": [],
      "uuid": "",
      "parent_uuid": null,
      "bundle_uuid": "",
      "bundle_viewid": "32bbce12-28dc-9c69-9207-c91b480ee915",
      "view_id": null,
      "view_position": [
        0,
        0,
        0
      ],
      "view_rotation": [
        0,
        0,
        0
      ]
    },
    {
      "version": "2.0",
      "type": 'fijnbezaagde hardhouten paal',
      "catalogue_id": 708,
      "configuration": {
        "materials": {},
        "attributes": []
      },
      "attributes": [
      ],
      "uuid": "",
      "parent_uuid": "",
      "bundle_uuid": "",
      "view_id": "",
      "view_position": [0, 0, 0],
      "view_rotation": [0, 0, 0],
      "place_holder": function(){
        var geometry = new THREE.BoxGeometry( 0.07, 1, 0.07 );//new THREE.CylinderGeometry( 0.035, 0.035, 1, 32 );//
        var material = new THREE.MeshBasicMaterial( {color: 0xFCFCFD, map: animatedTexture, transparent: true} );
        var cube = new THREE.Mesh( geometry, material );
        cube.position.set(0, 0.5, 0);
        animateTextureUVPosition();
        return cube;
      }
    },
    {
      "version": "2.0",
      "type": 'schutting4',
      "catalogue_id": 709,
      "configuration": {
        "materials": {},
        "attributes": []
      },
      "attributes": [
      ],
      "uuid": "",
      "parent_uuid": "",
      "bundle_uuid": "",
      "view_id": "",
      "view_position": [0, 0, 0],
      "view_rotation": [0, 0, 0],
      "place_holder": function(){
        var geometry = new THREE.BoxGeometry( 0.05, 1, 0.9 );
        var material = new THREE.MeshBasicMaterial( {color: 0xFFFFFF, map: animatedTexture, transparent: true} );
        var cube = new THREE.Mesh( geometry, material );
        cube.position.set(0, 0.5, -0.45);
        cube.onAfterRender = (renderer, scene, camera)=>{
          animateTextureUVPosition();
        };
        return cube;
      }
    },
    {
      "version": "2.0",
      "type": 'schutting5',
      "catalogue_id": 713,
      "configuration": {
        "materials": {},
        "attributes": []
      },
      "attributes": [
      ],
      "uuid": "",
      "parent_uuid": "",
      "bundle_uuid": "",
      "view_id": "",
      "view_position": [0, 0, 0],
      "view_rotation": [0, 0, 0],
      "place_holder": function(){
        var geometry = new THREE.BoxGeometry( 0.05, 1, 1.8 );
        var material = new THREE.MeshBasicMaterial( {color: 0xFFFFFF, map: animatedTexture, transparent: true} );
        var cube = new THREE.Mesh( geometry, material );
        cube.position.set(0, 0.5, -0.45);
        cube.onAfterRender = (renderer, scene, camera)=>{
          animateTextureUVPosition();
        };
        return cube;
      }
    }
  ];
})(window);
