(function(aNamespace){
  aNamespace.PlannerOptionMappings = {
    "height": {
      "1-to-1-2": {
        "begin": "1meter",
        "end": "1-2meter"
      },
      "1-2-to-1": {
        "begin": "1-2meter",
        "end": "1meter"
      }
    }
  };
})(window);
