var planner;
var canvas;

var nextBtn;
var postLengths, postTypes, postOffset, postImageUrls,
    fenceLengths, fenceTypes, fenceImageUrls,
    gateLengths, gateTypes, gateImageUrls;

var fencesContainer;
var fenceGatesContainer;
var fencePostsContainer;

var fenceButtons = [];
var fencePostButtons = [];
var fenceGateButtons = [];

var menuToggle;
var menuToggleArrow;

var sidemenu, containerTop;
var isMenuOpen = true;
var questionBtn,
    zoomInBtn,
    zoomOutBtn,
    centerBtn;

function init()
{
    menuToggle = document.getElementById("close_btn");
    menuToggleArrow = document.getElementById("toggleArrow");
    
    sidemenu = document.getElementById("sidebar-left");
    containerTop = document.getElementById("container-top");
    questionBtn = document.getElementById("help_btn");
    zoomInBtn = document.getElementById("zoom_in");
    zoomOutBtn = document.getElementById("zoom_out");
    
    fencesContainer = document.getElementById("fenceOptionsItems");
    fenceGatesContainer = document.getElementById("fencegateOptionsItems");
    fencePostsContainer = document.getElementById("fencepostOptionItems");

    menuToggle.onclick = () => {
        if (isMenuOpen) {
            sidemenu.classList.add("closed");
            menuToggle.classList.add("closed");
            menuToggleArrow.classList.add("closed");
            containerTop.classList.add("closed");
        } else {
            sidemenu.classList.remove("closed");
            menuToggle.classList.remove("closed");
            menuToggleArrow.classList.remove("closed");
            containerTop.classList.remove("closed");
        }
        isMenuOpen = !isMenuOpen;
    };
    questionBtn.onclick = () => {
        if (planner !== null) {
            planner.ShowIntroPopup();
        }
    };

    zoomInBtn.onclick = () => {
        if (planner !== null) {
            planner.ZoomIn();
        }
    };
    zoomOutBtn.onclick = () => {
        if (planner !== null) {
            planner.ZoomOut();
        }
    };

    planner = new Gardenplanner("gardenplanner-canvas", () => {
        InitData();

        var data = localStorage.getItem("planner");

        if (data != null && typeof(data) != "undefined")
        {
            planner.Load(data).then(() => {
                console.log("Loading export complete!");
                localStorage.removeItem("planner");
            }, (e) => {
                console.error("Something went wrong during loading json file! Error: ", e);
                localStorage.removeItem("planner");
            });
        }
    });
    /*moveBtn = document.getElementById("moveBtn");
    if (moveBtn) {
        moveBtn.onclick = onMoveBtnClick;
    }*/
    drawBtn = document.getElementById("drawBtn");
    if (drawBtn) {
        drawBtn.onclick = onDrawBtnClick;
    }
    /*selectBtn = document.getElementById("selectBtn");
    if (selectBtn) {
        selectBtn.onclick = onSelectBtnClick;
    }*/

    centerBtn = document.getElementById("centerBtn");
    if (centerBtn) {
        centerBtn.onclick = onCenterBtnClick;
    }

    nextBtn = document.getElementById("nextBtn");
    if (nextBtn) {
        nextBtn.onclick = onNextBtnClick;
    }

    //drawBtn.onclick(null);
}

/*function onMoveBtnClick(ev) {
    if (ev) ev.preventDefault();

    planner.SetAction(GardenplannerActions.MOVE);

    DeselectAllButtons();
    SelectButton(moveBtn);
}*/
function onDrawBtnClick(ev) {
    if (ev) ev.preventDefault();
    planner.FreeDrawing();

    DeselectAllButtons();
    SelectButton(drawBtn);
}
/*function onSelectBtnClick(ev) {
    if (ev) ev.preventDefault();
    planner.SetAction(GardenplannerActions.SELECT);

    DeselectAllButtons();
    SelectButton(selectBtn);
}*/

function onCenterBtnClick(ev) {
    if (ev) ev.preventDefault();
    planner.Center();
}
function onNextBtnClick(ev) {
    if (ev) ev.preventDefault();
    planner.Export().then((data) => {
        // Save export in local storage
        localStorage.setItem("planner", data);
        window.location.assign("./3D.html"); // redirect to 3D page
    }, (e) => {
        alert("Something went wrong! Try again later...");
    });
}
function SelectButton(button) {
    if (button && !button.classList.contains("selected")) {
        button.classList.add("selected");
    }
}

function DeselectAllButtons()
{
    /*if (moveBtn && moveBtn.classList.contains("selected")) {
        moveBtn.classList.remove("selected");
    }
    if (drawBtn && drawBtn.classList.contains("selected")) {
        drawBtn.classList.remove("selected");
    }
    if (selectBtn && selectBtn.classList.contains("selected")) {
        selectBtn.classList.remove("selected");
    }*/
}

function Export() {
    planner.Export().then((data) => {
        console.log("Export successful! Data: ", data);
    }, (error) => {
        console.error("Something went wrong during the export..", error);
    });
}
function InitData() {
    postLengths = planner.GetPostLengths();
    postTypes = planner.GetPostTypes();
    postOffset = planner.GetPostOffsets();
    postImageUrls = planner.GetPostImageUrls();

    fenceLengths = planner.GetFenceLengths();
    fenceTypes = planner.GetFenceTypes();
    fenceImageUrls = planner.GetFenceImageUrls();

    gateLengths = planner.GetGateLengths();
    gateTypes = planner.GetGateTypes();
    gateImageUrls = planner.GetGateImageUrls();

    let defaultPostLength = planner.GetDefaultPostLength();
    let defaultFenceLength = planner.GetDefaultFenceLength();
    let defaultGateLength = planner.GetDefaultGateLength();

    for (var i=0; i < postLengths.length; i++) {

        var button = addOptionButton(
            fencePostsContainer, 
            (postLengths[i] === defaultPostLength) ? "selected" : "", 
            postTypes[i],
            //postTypes[i] + " - " + postLengths[i],
            postLengths[i],
            postImageUrls[i]
        );
        button.addEventListener('click', (el, ev) => {
            for (var x=0; x < fencePostButtons.length; x++) {
                fencePostButtons[x].classList.remove("selected");
            }
            el.srcElement.classList.add("selected");
            var value = el.srcElement.getAttribute("data-value");
            planner.SetDefaultPost( Number.parseFloat(value) );
        }, false);
        fencePostButtons.push(button);

        /*let el = document.createElement("option");
        el.innerText = postTypes[i] + " - " + postLengths[i];
        el.setAttribute("value", postLengths[i]);
        if (postLengths[i] === defaultPostLength) {
            el.setAttribute("selected", "");
        }
        optionPosts.appendChild(el);*/
    }
    /*optionPosts.addEventListener("change", () => {
        planner.SetDefaultPost( Number.parseFloat(optionPosts.value) );
    });*/

    for (var i=0; i < fenceLengths.length; i++) {
        var button = addOptionButton(
            fencesContainer, 
            (fenceLengths[i] === defaultFenceLength) ? "selected" : "", 
            fenceTypes[i],
            //fenceTypes[i] + " - " + fenceLengths[i],
            fenceLengths[i],
            fenceImageUrls[i]
        )
        button.addEventListener('click', (el, ev) => {
            for (var x=0; x < fenceButtons.length; x++) {
                fenceButtons[x].classList.remove("selected");
            }
            el.srcElement.classList.add("selected");
            var value = el.srcElement.getAttribute("data-value");
            planner.SetDefaultFence( Number.parseFloat(value) );
        }, false);
        fenceButtons.push(button);

        /*let el = document.createElement("option");
        el.innerText = fenceTypes[i] + " - " + fenceLengths[i];
        el.setAttribute("value", fenceLengths[i]);
        if (fenceLengths[i] === defaultFenceLength) {
            el.setAttribute("selected", "");
        }
        optionFences.appendChild(el);*/
    }
    /*optionFences.addEventListener("change", () => {
        planner.SetDefaultFence( Number.parseFloat(optionFences.value) );
    });*/

    for (var i=0; i < gateLengths.length; i++) {
        var button = addOptionButton(
            fenceGatesContainer, 
            (gateLengths[i] === defaultGateLength) ? "selected" : "", 
            gateTypes[i],
            //gateTypes[i] + " - " + gateLengths[i],
            gateLengths[i],
            gateImageUrls[i]
        );
        button.addEventListener('click', (el, ev) => {
            for (var x=0; x < fenceGateButtons.length; x++) {
                fenceGateButtons[x].classList.remove("selected");
            }
            el.srcElement.classList.add("selected");
            var value = el.srcElement.getAttribute("data-value");
            planner.SetDefaultGate( Number.parseFloat(value) );
        }, false);
        fenceGateButtons.push(button);

        /*let el = document.createElement("option");
        el.innerText = gateTypes[i] + " - " + gateLengths[i];
        el.setAttribute("value", gateLengths[i]);
        if (gateLengths[i] === defaultGateLength) {
            el.setAttribute("selected", "");
        }
        optionGates.appendChild(el);*/
    }
    /*optionGates.addEventListener("change", () => {
        planner.SetDefaultGate( Number.parseFloat(optionGates.value) );
    });*/

}

function addOptionButton(container, classNames, text, value, imageUrl)
{
    // <div class="btn btn-fencepost">1</div>
    var button = document.createElement('div');
    if (classNames != "") {
        button.className = "btn btn-fenceoption " + classNames;
    } else {
        button.className = "btn btn-fenceoption";
    }
    button.setAttribute("data-value", value);
    
    var divImage = new Image(); //document.createElement('image');
    divImage.className = "btn-fenceoption-image";
    divImage.src = imageUrl;
    button.appendChild(divImage);
    
    var divText = document.createElement('span');
    divText.className = "btn-fenceoption-text";
    divText.innerText = text;
    button.appendChild(divText);
    
    container.appendChild(button);

    return button;
}

window.onload = init;